<?php
//Initialisation de l'affichage des erreurs
ini_set('display_errors', '1');

//Autoloader PSR-4
require_once("main.php");

//Aliases
use \hellokant\model\Model as Model;
use \hellokant\model\Article as Article;
use \hellokant\model\Categorie as Categorie;

echo("\r\n== Récupération de toutes les catégories ==\r\n");
$categories = Categorie::all();
echo "Toutes les catégories disponibles :\r\n";
afficherCategories($categories);

echo("\r\n== Insertion d'une catégorie ==\r\n");
$categorie = new Categorie(["nom" => "cd",
                            "descr" => "CD & DVD"]);
$categorie->insert();
echo("\r\n== Nouvelle catégorie ==\r\n");
afficherCategorie($categorie);

echo("\r\n== Suppression de la nouvelle catégorie ==\r\n");
$categorie->delete();

echo("\r\n== Récupération de la catégorie 1 ==\r\n");
$categorie = Categorie::first(1);
afficherCategorie($categorie);

echo("\r\n== Articles de la catégorie ==\r\n");
afficherArticles($categorie->article);



echo("\r\n== Récupération de tout les articles et affichage du nom seulement ==\r\n");
$articles = Article::all();
echo "Tous les articles disponibles :\r\n";
afficherArticles($articles);

echo("\r\n== Récupération des articles avec 'velo' dans la description ==\r\n");
$articles = Article::find(["descr", "LIKE", "%velo%"], ["id", "nom", "descr"]);
afficherArticles($articles);

echo("\r\n== Insertion d'un article ==\r\n");
$article = new Article(["nom" => "Trotinette électrique",
                                "descr" => "Trotinette pliable avec une grande autonnomie",
                                "tarif" => 50,
                                "id_categ" => 1]);

$article->insert();

afficherArticle($article);

echo("\r\n== Catégorie de l'article ==\r\n");
afficherCategorie($article->categorie);

echo("\r\n== Suppression de l'article==\r\n");
$article->delete();

?>