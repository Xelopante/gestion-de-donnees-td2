<?php
//Initialisation de l'affichage des erreurs
ini_set('display_errors', '1');

//Autoloader PSR-4
require_once("vendor/autoload.php");

//Aliases
use \hellokant\connection\ConnectionFactory as ConnectionFactory;

//Initialisation connexion PDO
$config = parse_ini_file("conf/config.ini");
ConnectionFactory::makeConnection($config);

function afficherArticle($article) {
    echo("ID de l'article : ".$article->id."\r\n");
    echo("Nom de l'article : ".$article->nom."\r\n");
    echo("Description : ".$article->descr."\r\n");
    echo("Tarif : ".$article->tarif."\r\n");
}

function afficherArticles($articles) {
    foreach($articles as $article) {
        afficherArticle($article);
        echo("-----------------------------------\r\n");
    }
}

function afficherCategorie($categorie) {
    echo("ID de la catégorie : ".$categorie->id."\r\n");
    echo("Nom de la catégorie : ".$categorie->nom."\r\n");
    echo("Description : ".$categorie->descr."\r\n");
}

function afficherCategories($categories) {
    foreach($categories as $categorie) {
        afficherCategorie($categorie);
        echo("-----------------------------------\r\n");
    }
}

echo("=== Script de test de la classe Query : ===\r\n");
include("query_script.php");
readline("Appuyez sur Entrée pour paser à la suite\r\n");
echo("=== Script de test de la classe Model : ===\r\n");
include("model_script.php");

?>