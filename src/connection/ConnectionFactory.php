<?php
namespace hellokant\connection;

class ConnectionFactory {

    private static $pdo;

    public static function makeConnection($config) {
        $params = array(\PDO::ATTR_ERRMODE=>\PDO::ERRMODE_EXCEPTION, \PDO::ATTR_EMULATE_PREPARES=>false, \PDO::ATTR_STRINGIFY_FETCHES => false, \PDO::ATTR_PERSISTENT=>true);
        //Configuration de la base de données
        self::$pdo = new \PDO("mysql:host=".$config["host"].";dbname=".$config["database"], $config["username"], $config["password"], $params);
    }

    public static function getConnection() {
        $connection = null;
        if(isset(self::$pdo)) {
            $connection = self::$pdo;
        }

        return $connection;
    }
}
?>