<?php
namespace hellokant\query;
use \hellokant\connection\ConnectionFactory as ConnectionFactory;

class Query {
    private $sqltable;
    private $fields = '*';
    private $where = null;
    private $args = [];
    private $sql = '';

    public function __construct() {
        //initialisation des attributs 
    }

    public static function table(string $nameTable) : Query {
        $query = new Query;
        $query->sqltable = $nameTable;
        return $query;
    }

    //Crée et retourne un SELECT SQL avec toutes les colonnes passées et paramètre + la table
    public function select($columns) {
        $this->sql = "SELECT ".implode(",", $columns)." FROM ".$this->sqltable;
        return $this;
    }

    //Fonction qui crée et retourne une condition WHERE en SQL
    public function where(string $column, string $operator, $val) : Query {
        //Si c'est le where a déjà été appelé, on ajoute un autre avec un operateur AND, sinon on déclare WHERE
        if($this->where !== null) {
            $this->where .= " AND $column $operator ?";
        }
        else {
            $this->where = "WHERE $column $operator ?";
        }
        $this->args[]=$val;
        return $this;
    }

    public function get() {
        $pdo = ConnectionFactory::getConnection();

        //Si select() n'a pas été appelé, on met tous les champs par défaut
        if($this->sql === '') {
            $this->sql = "SELECT ".$this->fields." FROM ".$this->sqltable;
        }
        
        $this->sql .= " ".$this->where;
        
        //préparation de la requête avec PDO
        $statement = $pdo->prepare($this->sql);

        $statement->execute($this->args);
        
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function delete() {
        $pdo = ConnectionFactory::getConnection();

        $this->sql = "DELETE FROM ".$this->sqltable." ".$this->where;

        //préparation de la requête avec PDO
        $statement = $pdo->prepare($this->sql);
        
        return $statement->execute($this->args);
    }

    //Crée une requête insert avec les champs passés en paramètres, insert l'objet dans la base et retourne son ID
    public function insert($fields) {
        $pdo = ConnectionFactory::getConnection();

        //Concaténation des clés du tableau par une virgule, puis bind des ? pour la valeur avec le nombre d'éléments du tableau -1 pour éviter la virgule à la fin (? ajouté manuellement)
        $this->sql = "INSERT INTO ".$this->sqltable." (".implode(",", array_keys($fields)).") VALUES (".str_repeat("?,", (count($fields)-1))."?)";

        //préparation de la requête avec PDO
        $statement = $pdo->prepare($this->sql);

        //On execute la requête avec les valeurs de chaque case du tableau
        $statement->execute(array_values($fields));

        return $pdo->lastInsertId();
    }
}
?>