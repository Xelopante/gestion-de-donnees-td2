<?php
namespace hellokant\model;
use \hellokant\model\Model as Model;

class Article extends Model {

    protected static $table = 'article';
    protected static $id_column = 'id';

    public function __construct(array $attributes = null) {
        parent::__construct($attributes);
    }

    public function categorie() {
        return $this->belongsTo('Categorie', 'id_categ');
    }

}

?>