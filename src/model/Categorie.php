<?php
namespace hellokant\model;
use \hellokant\model\Model as Model;

class Categorie extends Model {

    protected static $table = 'categorie';
    protected static $id_column = 'id';

    public function __construct(array $attributes = null) {
        parent::__construct($attributes);
    }

    public function articles() {
        return $this->hasMany('Article', 'id_categ');
    }

}

?>