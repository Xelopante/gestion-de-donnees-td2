<?php
namespace hellokant\model;
use \hellokant\query\Query as Query;

abstract class Model {

    protected static $table;
    protected static $id_column = 'id';

    protected $attributes = [];

    public function __construct(array $attributes = null) {
        if(!is_null($attributes)) {
            $this->attributes = $attributes;
        }
    }

    //On vérifie que le nom de l'attribut existe, sinon on verifie que le nom de la méthode existe
    public function __get(string $name) {
        if(array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        }
        else if(method_exists($this, $name)) {
            return $this->$name();
        }
    }

    public function __set($name, $value) : void {
        $this->attributes[$name] = $value;
    }

    //On récupère l'id de l'objet courant, puis on le supprime
    public function delete() {
        $id_field = static::$id_column;
        $id = $this->$id_field;

        if(isset($id) && $id !== null) {
            $query = Query::table(static::$table);
            
            return $query->where($id_field, '=', $id)->delete();
        }
    }

    //On vérifie si le champ d'ID est null (pour les auto-increment), puis on insert les attributs de l'objet courant
    public function insert() {
        $query = Query::table(static::$table);

        $id_field = static::$id_column;

        if(!is_null($this->$id_field)) {
            $this->$id_field = null;
        }

        $this->$id_field = $query->insert($this->attributes);
    }

    //On récupère toutes les lignes de la table, puis on les converti en objets de la classe utilisée
    public static function all() {
        $query = Query::table(static::$table);

        $array = $query->get();
        $object_array = array();

        if(!empty($array)) {
            foreach($array as $row) {
                $object = new static($row);
                $object_array[] = $object;
            }
        }

        return $object_array;
    }

    /*
     * On vérifie si les colonnes de select sont vide, si non on les applique à la requête
     * Si les champs du where sont un tableau :
     *    - si il n'y en a qu'un, on applique les champs une seule fois
     *    - si il y en a plusieurs, on boucle dessus et on les applique à chaque fois
     * Sinon on applique l'id passé en paramètre comme valeur du where
    */
    public static function find($fields = array(), $columns = array()) {
        $query = Query::table(static::$table);

        if(!empty($columns)) {
            $query->select($columns);
        }

        if(!empty($fields)) {
            if(is_array($fields)) {
                foreach($fields as $key => $field) {
                    if(is_array($field)) {
                        $query->where($fields[$key][0], $fields[$key][1], $fields[$key][2]);
                    }
                    else {
                        $query->where($fields[0], $fields[1], $fields[2]);
                    }
                }
            }
            else {
                $query->where(static::$id_column, '=', $fields);
            }
        }

        $array = $query->get();
        $object_array = array();
        
        if(!empty($array)) {
            foreach($array as $row) {
                $object = new static($row);
                $object_array[] = $object;
            }
        }

        return $object_array;
    }

    //On utilise la fonction find mais on retourne seulemenbt le 1er objet
    public static function first($fields = array(), $columns = array()) {

        return static::find($fields, $columns)[0];
    }

    //Retourne la ligne de la table associée demandée (cardinalité * -> 1), on convertit celle-ci en objet
    public function belongsTo($model, $foreign_key) {
        $model = "hellokant\model\\".$model;

        $query = Query::table($model::$table);

        $row = $query->where(static::$id_column, "=", $this->$foreign_key)->get()[0];
        $object = new $model($row);

        return $object;
    }

    //Retourne les lignes de la table associée demandée (cardinalité 1 -> *), on convertit celles-ci en objet
    public function hasMany($model, $foreign_key) {
        $model = "hellokant\model\\".$model;

        $query = Query::table($model::$table);

        $array = $query->where($foreign_key, "=", $this->id)->get();
        $object_array = array();
        
        if(!empty($array)) {
            foreach($array as $row) {
                $object = new $model($row);
                $object_array[] = $object;
            }
        }

        return $object_array;
    }
}

?>