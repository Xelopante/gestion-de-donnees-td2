<?php
//Initialisation de l'affichage des erreurs
ini_set('display_errors', '1');

//Autoloader PSR-4
require_once("main.php");

//Aliases
use \hellokant\query\Query as Query;

echo("\r\n== Récupération de tout les articles ==\r\n");
$query = Query::table('article');
$articles = $query->select(['*'])->get();
var_dump($articles);

echo("\r\n== Récupération de l'article avec l'ID 64 ==\r\n");
$query = Query::table('article');
$article = $query->where("id", "=", "64")->get();
var_dump($article);

echo("\r\n== Récupération de l'article avec le mot velo et bleu dans la description ==\r\n");
$query = Query::table('article');
$article = $query->where("descr", "LIKE", "%velo%")->where("descr", "LIKE", "%bleu%")->get();
var_dump($article);

echo("\r\n== Insertion d'un nouvel article ==\r\n");
$query = Query::table('article');
$id_article = $query->insert(["nom" => "banc de muscu", "descr"=>"banc de musculation pliable", "tarif" => 250, "id_categ" => 1]);
$query = Query::table('article');
$article = $query->where('id', '=', $id_article)->get();
var_dump($article);

echo("\r\n== Suppression du nouvel article ==\r\n");
$query = Query::table('article');
$query->where('id', '=', $id_article)->delete();


?>